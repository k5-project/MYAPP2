package com.demo.myapp2.api.v1;

import com.demo.myapp2.sdk.api.v1.api.OrdersApiV1Delegate;
import org.springframework.stereotype.Service;
import org.springframework.context.annotation.ComponentScan;
import com.demo.myapp2.sdk.api.v1.model.Order;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.context.request.NativeWebRequest;
import org.springframework.web.multipart.MultipartFile;

import java.util.List;
import java.util.Map;
import java.util.Optional;


/**
 * A stub that provides implementation for the OrdersApiV1Delegate
 */
@SuppressWarnings("unused")
@Service
@ComponentScan(basePackages = "com.demo.myapp2.sdk.api.v1.api")
public class OrdersApiV1Provider implements OrdersApiV1Delegate {

  @Override
  public ResponseEntity<List<Order>> getOrders() {
    //TODO Auto-generated method stub
    return ResponseEntity.ok(null);
  }

}
